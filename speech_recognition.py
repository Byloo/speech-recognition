#!/usr/bin/env python3
"""
# Developer: Bilel Hammami
# Email: Bilel.el.hammami@gmail.com

# NOTE: this script requires pocketsphinx library because it uses the Microphone class
"""

import pyaudio
from datetime import datetime
import os
from speech_recognition import Recognizer, Microphone, UnknownValueError, RequestError


class SpeechRecognizer(Microphone):
    """
    This class is used to detect audio input sources, allow user to change the default source
    and to record  and recognize user speech.
    """
    audio = pyaudio.PyAudio()

    def __init__(self, idx_device=None):
        if not idx_device:
            super().__init__()
        elif idx_device in SpeechRecognizer.get_all_devices().keys():
            super().__init__(device_index=idx_device)
        else:
            raise ValueError("L'index de préphénique audio est incorrect !")

    @staticmethod
    def listen(source):
        """
        This static method is used to record user speech.
        Input:
            source: audio input
        Output:
            audio record.
        """
        with source:
            return Recognizer().listen(source, timeout=3)

    @staticmethod
    def recognize(speech, language="fr-FR"):
        """
        This static method is used to recognize user speech.
        Input:
            speech: audio record from the selected audio input.
            language: the language of the speech
        Output:
            the speech in the text format.
        """
        try:
            text = Recognizer().recognize_sphinx(speech, language)
            print("vous avez dit: " + text)
            return text
        except UnknownValueError:
            print("Votre parole est incompréhensible")
        except RequestError as e:
            print("Sphinx erreur; {0}".format(e))

    @classmethod
    def get_all_devices(cls):
        """
        This class method to detect all audio devices.
        Output:
            devices_dict: a dictionary contain all detected audio devices.
        """
        nbr_device = cls.audio.get_device_count()
        devices_dict = dict()
        if nbr_device > 0:
            for i in range(nbr_device):
                device_name = cls.audio.get_device_info_by_index(i)['name']
                devices_dict[i] = device_name
            return devices_dict
        else:
            raise OSError("Aucun périphérique audio est trouvé!")

    @classmethod
    def get_default_input(cls):
        """
        This class method to detect all audio devices.
        Output:
            device_name: the name of the default audio input devices.
        """
        device_name = cls.audio.get_default_input_device_info()['name']
        return device_name


if __name__ == "__main__":
    # Detect audio inputs and the default one
    devices = SpeechRecognizer.get_all_devices()
    default_device = SpeechRecognizer.get_default_input()
    print("Les périphériques audio disponibles sont:")
    for i in devices.keys():
        print(f"{i}- {devices[i]}")
        if devices[i] == default_device:
            idx = i
    print(f'Le périphérique utilisé est "{default_device}".')

    # Asking user if he want to change the default audio input
    rps = input('Vous voulez le changer (Oui/Non)? :')
    if 'n' in rps.lower()[0] and len(rps) < 4:
        Speech = SpeechRecognizer(idx)
    elif 'o' in rps.lower()[0] and len(rps) < 4:
        idx = int(input("Veuillez saisir le numéro du préphénique sélectionner: "))
        Speech = SpeechRecognizer(idx)
    else:
        raise ValueError("Entrée invalide !")

    # Recording audio from the input source
    print("Disez quelque chose:")
    audio = SpeechRecognizer.listen(Speech)

    # Recognize speech using Sphinx
    try:
        text = SpeechRecognizer.recognize(audio, language="fr-FR")
        filename = datetime.now().strftime("speech_%Y-%m-%d_%H-%M") + ".txt"
        full_path = os.path.join("./Speeches", filename)
        with open(full_path, "w") as file:
            file.write(text)
            print(f'\nVotre parole est enregistré dans le fichier:\n"{os.path.abspath(full_path)}"')
    except UnknownValueError:
        print("Votre parole est incompréhensible")
    except RequestError as e:
        print("Sphinx error; {0}".format(e))
